// File: IController.ts

interface IController {
    renderViews():void;
}
export = IController;