// File: IGame.ts
/// <reference path="./INameable"/>
import INameable = require('./INameable');

interface IGame extends INameable {
    user?: INameable;
    status: string;
    field: string;
    genre: string;
    topScore: number;
}

export = IGame;