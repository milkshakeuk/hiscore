// File: DataInterfaces.ts
export interface IGameListItem {
    status: string;
    _last_tally_update_e: number;
    _top_longname:string;
    gamename: string;
    _top_shortname: string;
    field: string;
    longname: string;
    genre: string;
    _top_score: number;
}

export interface IActivityItem {
    _target: string;
    gamename: string;
    rank: number;
    score: number;
    dispunit: string;
    time: number;
    shortname: string;
    longname: string;
}

export interface IScoreBoardItem {
    shortname: string;
    score: number;
    longname: string;
    time: number;
}

export interface IScoreBoard {
    hi: number;
    scoreboard: IScoreBoardItem[];
    prid: string;
}
