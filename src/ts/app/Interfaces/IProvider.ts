// File: IProvider.ts

interface IProvider<T> {
    get():JQueryPromise<T>
}
export = IProvider;