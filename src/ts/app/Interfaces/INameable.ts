// File: INameable.ts

interface INameable {
    shortName: string;
    longName: string;
}

export = INameable;