// File: IActivity.ts
/// <reference path="INameable"/>

import INameable = require('INameable');

interface IActivity {
    user: INameable;
    target: string;
    gameName: string;
    displayUnit: string;
    dateTime: string;
    rank: number;
}

export = IActivity;