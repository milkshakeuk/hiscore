// File: UrlVariable.ts

class UrlVariable {
    value:string|number;
    constructor(public name:string, public type:string){}
}

export = UrlVariable;