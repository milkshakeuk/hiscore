// File: GameProvider.ts
/// <reference path="../../../../../typings/jquery/jquery.d.ts"/>
/// <reference path="../../../../../typings/underscore/underscore.d.ts"/>
/// <reference path="./BaseDataProvider"/>
/// <reference path="../../Models/Game"/>
/// <reference path="../../Interfaces/DataInterfaces"/>
/// <reference path="../../Interfaces/IProvider"/>

import _ = require('underscore');
import $ = require('jquery');
import BaseDataProvider = require('./BaseDataProvider');
import Game = require('../../Models/Game');
import DI = require('../../Interfaces/DataInterfaces');
import IProvider = require('../../Interfaces/IProvider');


class GameProvider extends BaseDataProvider implements IProvider<Game[]> {

    protected url:string = 'http://skeezix.wallednetworks.com:13001/curgamelist_1';

    public get():JQueryPromise<Game[]>{
        var deferred = $.Deferred();
        this.request().done((data) => {
            var result = _.map(data.gamelist, (item:DI.IGameListItem) => { return Game.createFromInterface(item); });
            deferred.resolve(result);
        })
        .fail(() => {
            deferred.reject();
        });

        return deferred.promise();
    }

}

export = GameProvider;