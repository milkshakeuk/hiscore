// File: ActivityProvider.ts
/// <reference path="../../../../../typings/jquery/jquery.d.ts"/>
/// <reference path="../../../../../typings/underscore/underscore.d.ts"/>
/// <reference path="./BaseDataProvider"/>
/// <reference path="../../Models/Activity"/>
/// <reference path="../../Interfaces/DataInterfaces"/>
/// <reference path="../../Interfaces/IProvider"/>

import _ = require('underscore');
import $ = require('jquery');
import BaseDataProvider = require('./BaseDataProvider');
import Activity = require('../../Models/Activity');
import DI = require('../../Interfaces/DataInterfaces');
import IProvider = require('../../Interfaces/IProvider');

class ActivityProvider extends BaseDataProvider implements IProvider<Activity[]> {

    protected url:string = 'http://skeezix.wallednetworks.com:13001/activityjson_1';

    public get():JQueryPromise<Activity[]>{
        var deferred = $.Deferred();
        this.request().done((data) => {
            var result = _.map(data, (item:DI.IActivityItem) => { return Activity.createFromInterface(item); });
            deferred.resolve(result);
        })
        .fail(() => {
            deferred.reject();
        });

        return deferred.promise();
    }

}

export = ActivityProvider;