// File: DataProvider.ts
/// <reference path="../../../../../typings/jquery/jquery.d.ts"/>
import $ = require('jquery');

class BaseDataProvider {

    protected url:string;

    constructor(){}

    protected request():JQueryXHR{
        if(typeof this.url !== 'string' || this.url === ""){
            throw new Error('url is required');
        }
        return $.ajax({url: this.url, dataType: 'jsonp', jsonp: 'jsonp'});
    }
}

export = BaseDataProvider;