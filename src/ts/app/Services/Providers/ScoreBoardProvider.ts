// File: ScoreBoardProvider.ts
/// <reference path="../../../../../typings/jquery/jquery.d.ts"/>
/// <reference path="../../../../../typings/underscore/underscore.d.ts"/>
/// <reference path="./BaseDataProvider"/>
/// <reference path="../../Models/ScoreBoard"/>
/// <reference path="../../Interfaces/DataInterfaces"/>
/// <reference path="../../Interfaces/IProvider"/>

import _ = require('underscore');
import $ = require('jquery');
import BaseDataProvider = require('./BaseDataProvider');
import ScoreBoard = require('../../Models/ScoreBoard');
import DI = require('../../Interfaces/DataInterfaces');
import IProvider = require('../../Interfaces/IProvider');


class ScoreBoardProvider extends BaseDataProvider implements IProvider<ScoreBoard[]> {

    constructor(gamename:string){
        super();
        this.game(gamename);
    }

    public game(gamename:string):void{
        this.url = `http://skeezix.wallednetworks.com:13001/json_1/${gamename}/`;
    }

    public get():JQueryPromise<ScoreBoard[]>{
        var deferred = $.Deferred();
        this.request().done((data) => {
            data = _.filter(data.scoreboard, (item:DI.IScoreBoardItem) => { return item.shortname !== ''; });
            var result = _.map(data, (item:DI.IScoreBoard) => { return ScoreBoard.createFromInterface(item); });
            deferred.resolve(result);
        })
        .fail(() => {
            deferred.reject();
        });

        return deferred.promise();
    }

}

export = ScoreBoardProvider;