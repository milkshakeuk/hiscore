// File: user.ts
/// <reference path="../Interfaces/INameable"/>

import INameable = require('../Interfaces/INameable');

class User implements INameable {

    public shortName:string
    public longName:string

    constructor(shortName:string, longName:string) {
        this.shortName = shortName;
        this.longName = longName;
    }
}
export = User;