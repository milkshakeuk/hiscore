// File: Activity.ts
/// <reference path="../../../../typings/moment/moment.d.ts"/>
/// <reference path="../Interfaces/INameable"/>
/// <reference path="../Interfaces/IActivity"/>
/// <reference path="../Interfaces/DataInterfaces"/>
/// <reference path="./User"/>

import moment = require('moment');
import INameable = require('../Interfaces/INameable');
import IActivity = require('../Interfaces/IActivity');
import DI = require('../Interfaces/DataInterfaces');
import User = require('./User');

class Activity implements IActivity {

    public user:INameable;

    public get dateTime():string {
        return moment(this._dateTime.toString(), 'X').format('YYYY/MM/DD hh:mm:ss');
    }

    constructor(shortName:string, longName:string, public rank:number, public target:string, public gameName:string,
                public displayUnit:string, private _dateTime:number) {
        this.user = new User(shortName, longName);
    }

    static createFromInterface(item:DI.IActivityItem):Activity {
        return new Activity(item.shortname, item.longname, item.rank, item._target, item.gamename, item.dispunit,
            item.time);
    }
}

export = Activity;