// File: Game.ts
/// <reference path="../Interfaces/INameable"/>
/// <reference path="../Interfaces/IGame"/>
/// <reference path="../Interfaces/DataInterfaces"/>
/// <reference path="./User"/>

import INameable = require('../Interfaces/INameable');
import IGame = require('../Interfaces/IGame');
import DI = require('../Interfaces/DataInterfaces');
import User = require('./User');

class Game implements IGame {

    public user: INameable;

    constructor(userShortName:string, userLongName:string, public status:string, public field:string,
                public genre:string, public topScore:number, public shortName:string, public longName:string) {
        this.user = new User(userShortName, userLongName);
    }

    static createFromInterface(item:DI.IGameListItem):Game {
        return new Game(item._top_shortname, item._top_longname, item.status, item.field, item.genre, item._top_score,
            item.gamename, item.longname);
    }
}
export = Game;