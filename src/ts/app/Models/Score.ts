// File: Score.ts
/// <reference path="../../../../typings/moment/moment.d.ts"/>
/// <reference path="../Interfaces/INameable"/>
/// <reference path="../Interfaces/DataInterfaces"/>
/// <reference path="./User"/>

import moment = require('moment');
import INameable = require('../Interfaces/INameable');
import DI = require('../Interfaces/DataInterfaces');
import User = require('./User');

class Score {

    public user: INameable;

    public get dateTime():string {
        return moment(this._dateTime.toString(), 'X').format('YYYY/MM/DD hh:mm:ss');
    }

    constructor(shortName:string, longName:string, public score:number, private _dateTime:number){
        this.user = new User(shortName, longName);
    }

    static createFromInterface(item:DI.IScoreBoardItem):Score {
        return new Score(item.shortname, item.longname, item.score, item.time);
    }
}
export = Score;