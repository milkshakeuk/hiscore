// File: ScoreBoard.ts
/// <reference path="../Interfaces/DataInterfaces"/>
/// <reference path="./Score"/>
/// <reference path="../../../../typings/underscore/underscore.d.ts"/>

import DI = require('../Interfaces/DataInterfaces');
import Score = require('./Score');
import _ = require('underscore');

class ScoreBoard {

    constructor(public hiScore: number, public scores:Score[]){}

    static createFromInterface(item: DI.IScoreBoard):ScoreBoard {
        var scores:Score[] = _.map(item.scoreboard, (item) => { return Score.createFromInterface(item) });
        return new ScoreBoard(item.hi, scores);
    }
}
export = ScoreBoard;