// File: ActivityProvider.ts
/// <reference path="../../../../../typings/jquery/jquery.d.ts"/>
/// <reference path="../../../../../typings/underscore/underscore.d.ts"/>
/// <reference path="./BaseDataProvider"/>
/// <reference path="../../Models/Activity"/>
/// <reference path="../../Interfaces/DataInterfaces"/>
/// <reference path="../../Interfaces/IProvider"/>
var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
define(["require", "exports", 'underscore', 'jquery', './BaseDataProvider', '../../Models/Activity'], function (require, exports, _, $, BaseDataProvider, Activity) {
    var ActivityProvider = (function (_super) {
        __extends(ActivityProvider, _super);
        function ActivityProvider() {
            _super.apply(this, arguments);
            this.url = 'http://skeezix.wallednetworks.com:13001/activityjson_1';
        }
        ActivityProvider.prototype.get = function () {
            var deferred = $.Deferred();
            this.request().done(function (data) {
                var result = _.map(data, function (item) { return Activity.createFromInterface(item); });
                deferred.resolve(result);
            })
                .fail(function () {
                deferred.reject();
            });
            return deferred.promise();
        };
        return ActivityProvider;
    })(BaseDataProvider);
    return ActivityProvider;
});
