define(["require", "exports", 'jquery'], function (require, exports, $) {
    var BaseDataProvider = (function () {
        function BaseDataProvider() {
        }
        BaseDataProvider.prototype.request = function () {
            if (typeof this.url !== 'string' || this.url === "") {
                throw new Error('url is required');
            }
            return $.ajax({ url: this.url, dataType: 'jsonp', jsonp: 'jsonp' });
        };
        return BaseDataProvider;
    })();
    return BaseDataProvider;
});
