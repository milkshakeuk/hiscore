// File: GameProvider.ts
/// <reference path="../../../../../typings/jquery/jquery.d.ts"/>
/// <reference path="../../../../../typings/underscore/underscore.d.ts"/>
/// <reference path="./BaseDataProvider"/>
/// <reference path="../../Models/Game"/>
/// <reference path="../../Interfaces/DataInterfaces"/>
/// <reference path="../../Interfaces/IProvider"/>
var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
define(["require", "exports", 'underscore', 'jquery', './BaseDataProvider', '../../Models/Game'], function (require, exports, _, $, BaseDataProvider, Game) {
    var GameProvider = (function (_super) {
        __extends(GameProvider, _super);
        function GameProvider() {
            _super.apply(this, arguments);
            this.url = 'http://skeezix.wallednetworks.com:13001/curgamelist_1';
        }
        GameProvider.prototype.get = function () {
            var deferred = $.Deferred();
            this.request().done(function (data) {
                var result = _.map(data.gamelist, function (item) { return Game.createFromInterface(item); });
                deferred.resolve(result);
            })
                .fail(function () {
                deferred.reject();
            });
            return deferred.promise();
        };
        return GameProvider;
    })(BaseDataProvider);
    return GameProvider;
});
