// File: ScoreBoardProvider.ts
/// <reference path="../../../../../typings/jquery/jquery.d.ts"/>
/// <reference path="../../../../../typings/underscore/underscore.d.ts"/>
/// <reference path="./BaseDataProvider"/>
/// <reference path="../../Models/ScoreBoard"/>
/// <reference path="../../Interfaces/DataInterfaces"/>
/// <reference path="../../Interfaces/IProvider"/>
var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
define(["require", "exports", 'underscore', 'jquery', './BaseDataProvider', '../../Models/ScoreBoard'], function (require, exports, _, $, BaseDataProvider, ScoreBoard) {
    var ScoreBoardProvider = (function (_super) {
        __extends(ScoreBoardProvider, _super);
        function ScoreBoardProvider(gamename) {
            _super.call(this);
            this.game(gamename);
        }
        ScoreBoardProvider.prototype.game = function (gamename) {
            this.url = "http://skeezix.wallednetworks.com:13001/json_1/" + gamename + "/";
        };
        ScoreBoardProvider.prototype.get = function () {
            var deferred = $.Deferred();
            this.request().done(function (data) {
                data = _.filter(data.scoreboard, function (item) { return item.shortname !== ''; });
                var result = _.map(data, function (item) { return ScoreBoard.createFromInterface(item); });
                deferred.resolve(result);
            })
                .fail(function () {
                deferred.reject();
            });
            return deferred.promise();
        };
        return ScoreBoardProvider;
    })(BaseDataProvider);
    return ScoreBoardProvider;
});
