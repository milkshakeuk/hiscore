// File: Activity.ts
/// <reference path="../../../../typings/moment/moment.d.ts"/>
/// <reference path="../Interfaces/INameable"/>
/// <reference path="../Interfaces/IActivity"/>
/// <reference path="../Interfaces/DataInterfaces"/>
/// <reference path="./User"/>
define(["require", "exports", 'moment', './User'], function (require, exports, moment, User) {
    var Activity = (function () {
        function Activity(shortName, longName, rank, target, gameName, displayUnit, _dateTime) {
            this.rank = rank;
            this.target = target;
            this.gameName = gameName;
            this.displayUnit = displayUnit;
            this._dateTime = _dateTime;
            this.user = new User(shortName, longName);
        }
        Object.defineProperty(Activity.prototype, "dateTime", {
            get: function () {
                return moment(this._dateTime.toString(), 'X').format('YYYY/MM/DD hh:mm:ss');
            },
            enumerable: true,
            configurable: true
        });
        Activity.createFromInterface = function (item) {
            return new Activity(item.shortname, item.longname, item.rank, item._target, item.gamename, item.dispunit, item.time);
        };
        return Activity;
    })();
    return Activity;
});
