// File: ScoreBoard.ts
/// <reference path="../Interfaces/DataInterfaces"/>
/// <reference path="./Score"/>
/// <reference path="../../../../typings/underscore/underscore.d.ts"/>
define(["require", "exports", './Score', 'underscore'], function (require, exports, Score, _) {
    var ScoreBoard = (function () {
        function ScoreBoard(hiScore, scores) {
            this.hiScore = hiScore;
            this.scores = scores;
        }
        ScoreBoard.createFromInterface = function (item) {
            var scores = _.map(item.scoreboard, function (item) { return Score.createFromInterface(item); });
            return new ScoreBoard(item.hi, scores);
        };
        return ScoreBoard;
    })();
    return ScoreBoard;
});
