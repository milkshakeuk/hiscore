// File: user.ts
/// <reference path="../Interfaces/INameable"/>
define(["require", "exports"], function (require, exports) {
    var User = (function () {
        function User(shortName, longName) {
            this.shortName = shortName;
            this.longName = longName;
        }
        return User;
    })();
    return User;
});
