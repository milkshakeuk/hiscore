// File: Game.ts
/// <reference path="../Interfaces/INameable"/>
/// <reference path="../Interfaces/IGame"/>
/// <reference path="../Interfaces/DataInterfaces"/>
/// <reference path="./User"/>
define(["require", "exports", './User'], function (require, exports, User) {
    var Game = (function () {
        function Game(userShortName, userLongName, status, field, genre, topScore, shortName, longName) {
            this.status = status;
            this.field = field;
            this.genre = genre;
            this.topScore = topScore;
            this.shortName = shortName;
            this.longName = longName;
            this.user = new User(userShortName, userLongName);
        }
        Game.createFromInterface = function (item) {
            return new Game(item._top_shortname, item._top_longname, item.status, item.field, item.genre, item._top_score, item.gamename, item.longname);
        };
        return Game;
    })();
    return Game;
});
