// File: Score.ts
/// <reference path="../../../../typings/moment/moment.d.ts"/>
/// <reference path="../Interfaces/INameable"/>
/// <reference path="../Interfaces/DataInterfaces"/>
/// <reference path="./User"/>
define(["require", "exports", 'moment', './User'], function (require, exports, moment, User) {
    var Score = (function () {
        function Score(shortName, longName, score, _dateTime) {
            this.score = score;
            this._dateTime = _dateTime;
            this.user = new User(shortName, longName);
        }
        Object.defineProperty(Score.prototype, "dateTime", {
            get: function () {
                return moment(this._dateTime.toString(), 'X').format('YYYY/MM/DD hh:mm:ss');
            },
            enumerable: true,
            configurable: true
        });
        Score.createFromInterface = function (item) {
            return new Score(item.shortname, item.longname, item.score, item.time);
        };
        return Score;
    })();
    return Score;
});
