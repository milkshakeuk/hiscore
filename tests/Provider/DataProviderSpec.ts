// File: DataProviderSpec.ts
/// <reference path="../../typings/jasmine/jasmine.d.ts"/>
/// <reference path="../../src/ts/app/Services/Providers/ActivityProvider"/>
/// <reference path="../../src/ts/app/Services/Providers/GameProvider"/>
/// <reference path="../../src/ts/app/Services/Providers/ScoreBoardProvider"/>
/// <reference path="../../src/ts/app/Models/Activity"/>
/// <reference path="../../src/ts/app/Models/Game"/>
/// <reference path="../../src/ts/app/Models/ScoreBoard"/>
/// <reference path="../../src/ts/app/Interfaces/IProvider"/>

import ActivityProvider = require('../../src/ts/app/Services/Providers/ActivityProvider');
import GameProvider = require('../../src/ts/app/Services/Providers/GameProvider');
import ScoreBoardProvider = require('../../src/ts/app/Services/Providers/ScoreBoardProvider');
import Activity = require('../../src/ts/app/Models/Activity');
import Game = require('../../src/ts/app/Models/Game');
import ScoreBoard = require('../../src/ts/app/Models/ScoreBoard');
import IProvider = require('../../src/ts/app/Interfaces/IProvider');

describe("Provider Suite", () => {

    var provider: IProvider<any>;

    it("should return requested activity data", (done) => {

        provider = new ActivityProvider();

        provider.get().done((data:Activity[]) => {
            expect(data.length).toBeGreaterThan(0);
        }).always(done);

    }, 10000);

    it("should return requested game list data", (done) => {

        provider = new GameProvider();

        provider.get().done((data:Game[]) => {
            expect(data.length).toBeGreaterThan(0);
        }).always(done);

    }, 10000);

    it("should return requested scoreboard data", (done) => {

        provider = new ScoreBoardProvider('mrrescue');

        provider.get().done((data:ScoreBoard[]) => {
            expect(data.length).toBeGreaterThan(0);
        }).always(done);

    }, 10000);

});